![1_JnpUndSzI4_nu-OR7KAsmg](../wikis/uploads/6be7275d11e45660c05d026f8f80cf85/1_JnpUndSzI4_nu-OR7KAsmg.jpeg)

# Utilisation

<pre><code>stegano est un programme permettant de cacher des informations dans une image.

  Utilisation:
    java -jar stegano.jar [-h | --help]
    java -jar stegano.jar (c | cacher) &lt;src&gt; &lt;cch&gt; &lt;dst&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt; &lt;rCch&gt; &lt;gCch&gt; &lt;bCch&gt;)]
    java -jar stegano.jar (r | retrouver) &lt;mod&gt; &lt;dst&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt; &lt;rCch&gt; &lt;gCch&gt; &lt;bCch&gt;)]
    java -jar stegano.jar (q | qualite) &lt;ref&gt; &lt;mod&gt; [Options]
    java -jar stegano.jar (g | gris) &lt;src&gt; &lt;dst&gt;
    java -jar stegano.jar (e | espace) &lt;src&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt;)]
    java -jar stegano.jar (cb | cacher_binaire) &lt;src&gt; &lt;bnr&gt; &lt;dst&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt;)] [(--Base64 | -b64)]
    java -jar stegano.jar (cm | cacher_message) &lt;src&gt; &lt;dst&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt;)] [(--Base64 | -b64)]
    java -jar stegano.jar (rb | retrouver_binaire) &lt;mod&gt; &lt;dst&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt;)] [(--Base64 | -b64)]
    java -jar stegano.jar (rm | retrouver_message) &lt;mod&gt; [(&lt;rSrc&gt; &lt;gSrc&gt; &lt;bSrc&gt;)] [(--Base64 | -b64)]
  
  Commandes:
    -h, --help                Affiche l'aide et termine l'exécution.
    c, cacher                 Cache une image (méthode LSB)
    r, retrouver              Retrouve une image cachée (méthode LSB)
    q, qualite                Affiche les mesures de qualité
    g, gris                   Convertit une image en niveau de gris
    e, espace                 Affiche l'espace disponible dans une image
    cb, cacher_binaire        Cache un binaire dans une image
    cm, cacher_message        Entre en mode saisie de texte et cache le message entré dans une image
    rb, retrouver_binaire     Retrouve un binaire cachée
    rm, retrouver_message     Retrouve et affiche le texte caché
  
  Options:
    -N &lt;fenetre&gt;, --fenetre &lt;fenetre&gt;   Taille des fenêtres pour la SSIM [défaut: 11].
    -a &lt;alpha&gt;, --alpha &lt;alpha&gt;         Paramètre d'ajustement de la luminance [défaut: 1.0].
    -b &lt;beta&gt;, --beta &lt;beta&gt;            Paramètre d'ajustement du contraste [défaut: 1.0].
    -g &lt;gamma&gt;, --gamma &lt;gamma&gt;         Paramètre d'ajustement de la structure [défaut: 1.0].
    -s &lt;sigma&gt;, --sigma &lt;sigma&gt;         Ecart-type de la distribution gaussienne normalisée appliquée sur chaque fenêtre [défaut: 1.5].
    -K1 &lt;K1&gt;, --K1 &lt;K1&gt;                 Constante du calcul de la SSIM [défaut: 0.01].
    -K2 &lt;K2&gt;, --K2 &lt;K2&gt;                 Constante du calcul de la SSIM [défaut: 0.03].
    -k &lt;k&gt;, --k &lt;k&gt;                     Constante de pondération de la CSSIM [défaut: 45.0].

  Autres options:
    -b64, --Base64                      Active l'encodage ou le décodage en Base64
    
  Arguments positionnels:
    src             Chemin vers l'image source
    cch             Chemin vers l'image à cacher
    bnr             Chemin vers le binaire à cacher
    ref             Chemin vers l'image de référence
    mod             Chemin vers l'image traitée
    dst             Chemin de destination de l'image traitée
    rSrc            Nombre de LSB sur la couche rouge de l'image source [défaut: 3]
    gSrc            Nombre de LSB sur la couche verte de l'image source [défaut: 3]
    bSrc            Nombre de LSB sur la couche bleue de l'image source [défaut: 3]
    rCch            Nombre de MSB de la couche rouge de l'image à cacher [défaut: 3]
    gCch            Nombre de MSB de la couche verte de l'image à cacher [défaut: 3]
    bCch            Nombre de MSB de la couche bleue de l'image à cacher [défaut: 3]</code></pre>

# Exemples d'utilisations

Des exemples complets d'utilisations sont disponibles dans la section **Travail réalisé et utilisation** du rapport.

# Compilation

La compilation du projet nécessite sbt. Le projet utilise Scala 2.13.1.

# Liens utiles

[Sujet](../wikis/uploads/101395536b855275ff3d4ce5dc0f5ad6/Sujet.pdf)
