
import com.tncy.top.image.ImageWrapper
import helper.{MeasureHelper, ColorHelper, FindHelper, HideHelper}
import java.nio.file.{Paths, Files}
import java.io.{FileOutputStream, BufferedOutputStream}
import java.nio.charset.StandardCharsets

object Main {
  implicit class OpsNum(val str: String) extends AnyVal {
    def isInt() = scala.util.Try(str.toInt).isSuccess
    def isPositiveInt() = scala.util.Try(str.toInt).isSuccess && str.toInt > 0
    def isPositiveOrZeroInt() = scala.util.Try(str.toInt).isSuccess && str.toInt >= 0
    def isDouble() = scala.util.Try(str.toDouble).isSuccess
    def isPositiveDouble() = scala.util.Try(str.toDouble).isSuccess && str.toDouble > 0
    def isPositiveOrZeroDouble() = scala.util.Try(str.toDouble).isSuccess && str.toDouble >= 0
  }

  val usage = """stegano est un programme permettant de cacher des informations dans une image.

  Utilisation:
    stegano [-h | --help]
    stegano (c | cacher) <src> <cch> <dst> [(<rSrc> <gSrc> <bSrc> <rCch> <gCch> <bCch>)]
    stegano (r | retrouver) <mod> <dst> [(<rSrc> <gSrc> <bSrc> <rCch> <gCch> <bCch>)]
    stegano (q | qualite) <ref> <mod> [Options]
    stegano (g | gris) <src> <dst>
    stegano (e | espace) <src> [(<rSrc> <gSrc> <bSrc>)]
    stegano (cb | cacher_binaire) <src> <bnr> <dst> [(<rSrc> <gSrc> <bSrc>)] [(--Base64 | -b64)]
    stegano (cm | cacher_message) <src> <dst> [(<rSrc> <gSrc> <bSrc>)] [(--Base64 | -b64)]
    stegano (rb | retrouver_binaire) <mod> <dst> [(<rSrc> <gSrc> <bSrc>)] [(--Base64 | -b64)]
    stegano (rm | retrouver_message) <mod> [(<rSrc> <gSrc> <bSrc>)] [(--Base64 | -b64)]
  
  Commandes:
    -h, --help                Affiche l'aide et termine l'exécution.
    c, cacher                 Cache une image (méthode LSB)
    r, retrouver              Retrouve une image cachée (méthode LSB)
    q, qualite                Affiche les mesures de qualité
    g, gris                   Convertit une image en niveau de gris
    e, espace                 Affiche l'espace disponible dans une image
    cb, cacher_binaire        Cache un binaire dans une image
    cm, cacher_message        Entre en mode saisie de texte et cache le message entré dans une image
    rb, retrouver_binaire     Retrouve un binaire cachée
    rm, retrouver_message     Retrouve et affiche le texte caché
  
  Options:
    -N <fenetre>, --fenetre <fenetre>   Taille des fenêtres pour le SSIM [défaut: 11].
    -a <alpha>, --alpha <alpha>         Paramètre d'ajustement de la luminance [défaut: 1.0].
    -b <beta>, --beta <beta>            Paramètre d'ajustement du contraste [défaut: 1.0].
    -g <gamma>, --gamma <gamma>         Paramètre d'ajustement de la similarité structurale [défaut: 1.0].
    -s <sigma>, --sigma <sigma>         Ecart-type de la distribution gaussienne normalisée appliquée sur chaque fenêtre [défaut: 1.5].
    -K1 <K1>, --K1 <K1>                 Constante du calcul du SSIM [défaut: 0.01].
    -K2 <K2>, --K2 <K2>                 Constante du calcul du SSIM [défaut: 0.03].
    -k <k>, --k <k>                     Constante de pondération du CSSIM [défaut: 45.0].

  Autres options:
    -b64, --Base64                      Active l'encodage ou le décodage en Base64
    
  Arguments positionnels:
    src             Chemin vers l'image source
    cch             Chemin vers l'image à cacher
    bnr             Chemin vers le binaire à cacher
    ref             Chemin vers l'image de référence
    mod             Chemin vers l'image traitée
    dst             Chemin de destination de l'image traitée
    rSrc            Nombre de LSB sur la couche rouge de l'image source [défaut: 3]
    gSrc            Nombre de LSB sur la couche verte de l'image source [défaut: 3]
    bSrc            Nombre de LSB sur la couche bleue de l'image source [défaut: 3]
    rCch            Nombre de MSB de la couche rouge de l'image à cacher [défaut: 3]
    gCch            Nombre de MSB de la couche verte de l'image à cacher [défaut: 3]
    bCch            Nombre de MSB de la couche bleue de l'image à cacher [défaut: 3]"""
  def main(args: Array[String]) {
    if (args.length == 0) { // Si aucun argument n'est spécifié, on affiche l'aide (on veut au moins une commande)
      println("Veuillez spécifier une commande")
      println(usage)
      sys.exit(1)
    }
    val argList = args.toList // On convertit les arguments en liste
    type OptionMap = Map[Symbol, Any] // On définit une nouvelle Map de Symbols avec des valeurs de types quelconques

    // On parse les arguments récursivement
    def optionMap(map : OptionMap, list: List[String]) : OptionMap = {
      list match {
        case Nil => map
        // Commandes
        case ("--help" | "-h") :: tail =>
          println(usage)
          sys.exit(0)
        case ("cacher" | "c") :: src :: cch :: dst :: rSrc :: gSrc :: bSrc :: rCch :: gCch :: bCch :: tail =>
          optionMap(map ++ Map('cacher -> true, 'ref -> src, 'cch -> cch, 'mod -> dst, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc, 'rCch -> rCch, 'gCch -> gCch, 'bCch -> bCch), tail)
        case ("cacher" | "c") :: src :: cch :: dst :: tail =>
          optionMap(map ++ Map('cacher -> true, 'ref -> src, 'cch -> cch, 'mod -> dst), tail)
        case ("retrouver" | "r") :: mod :: dst :: rSrc :: gSrc :: bSrc :: rCch :: gCch :: bCch :: tail =>
          optionMap(map ++ Map('retrouver -> true, 'ref -> mod, 'cch -> dst, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc, 'rCch -> rCch, 'gCch -> gCch, 'bCch -> bCch), tail)
        case ("retrouver" | "r") :: mod :: dst :: tail =>
          optionMap(map ++ Map('retrouver -> true, 'ref -> mod, 'cch -> dst), tail)
        case ("qualite" | "q") :: ref :: mod :: tail =>
          optionMap(map ++ Map('qualite -> true, 'ref -> ref, 'mod -> mod), tail)
        case ("gris" | "g") :: src :: dst :: tail =>
          optionMap(map ++ Map('gris -> true, 'ref -> src, 'mod -> dst), tail)
        case ("espace" | "e") :: src :: rSrc :: gSrc :: bSrc :: tail =>
          optionMap(map ++ Map('espace -> true, 'ref -> src, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc), tail)
        case ("espace" | "e") :: src :: tail =>
          optionMap(map ++ Map('espace -> true, 'ref -> src), tail)
        case ("cacher_binaire" | "cb") :: src :: bnr :: dst :: rSrc :: gSrc :: bSrc :: tail =>
          optionMap(map ++ Map('cacher_binaire -> true, 'ref -> src, 'cch -> bnr, 'mod -> dst, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc), tail)
        case ("cacher_binaire" | "cb") :: src :: bnr :: dst :: tail =>
          optionMap(map ++ Map('cacher_binaire -> true, 'ref -> src, 'cch -> bnr, 'mod -> dst), tail)
        case ("cacher_message" | "cm") :: src :: dst :: rSrc :: gSrc :: bSrc :: tail =>
          optionMap(map ++ Map('cacher_message -> true, 'ref -> src, 'mod -> dst, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc), tail)
        case ("cacher_message" | "cm") :: src :: dst :: tail =>
          optionMap(map ++ Map('cacher_message -> true, 'ref -> src, 'mod -> dst), tail)
        case ("retrouver_binaire" | "rb") :: mod :: dst :: rSrc :: gSrc :: bSrc :: tail =>
          optionMap(map ++ Map('retrouver_binaire -> true, 'ref -> mod, 'cch -> dst, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc), tail)
        case ("retrouver_binaire" | "rb") :: mod :: dst :: tail =>
          optionMap(map ++ Map('retrouver_binaire -> true, 'ref -> mod, 'cch -> dst, 'mod -> dst), tail)
        case ("retrouver_message" | "rm") :: mod :: rSrc :: gSrc :: bSrc :: tail =>
          optionMap(map ++ Map('retrouver_message -> true, 'ref -> mod, 'rSrc -> rSrc, 'gSrc -> gSrc, 'bSrc -> bSrc), tail)
        case ("retrouver_message" | "rm") :: mod :: tail =>
          optionMap(map ++ Map('retrouver_message -> true, 'ref -> mod), tail)
        // Options
        case ("--fenetre" | "-N") :: n :: tail =>
          optionMap(map ++ Map('fenetre -> n), tail)
        case ("--alpha" | "-a") :: alpha :: tail =>
          optionMap(map ++ Map('alpha -> alpha), tail)
        case ("--beta" | "-b") :: beta :: tail =>
          optionMap(map ++ Map('beta -> beta), tail)
        case ("--gamma" | "-g") :: gamma :: tail =>
          optionMap(map ++ Map('gamma -> gamma), tail)
        case ("--K1" | "-K1") :: k1 :: tail =>
          optionMap(map ++ Map('K1 -> k1), tail)
        case ("--K2" | "-K2") :: k2 :: tail =>
          optionMap(map ++ Map('K2 -> k2), tail)
        case ("--sigma" | "-s") :: sigma :: tail =>
          optionMap(map ++ Map('sigma -> sigma), tail)
        case ("--k" | "-k") :: k :: tail =>
          optionMap(map ++ Map('k -> k), tail)

        case ("--Base64" | "-b64") :: tail =>
          optionMap(map ++ Map('b64 -> true), tail)
        // Argument ou pattern non reconnu
        case option :: tail =>
          println("L'argument " + option + " ou le pattern n'est pas reconnu")
          println(usage)
          sys.exit(1)
      }
    }
    val options = optionMap(Map(),argList) // On appelle le parseur

    // Validation des arguments
    // On vérifie qu'une seule commande a été demandé
    var compte = 0
    for (x <- Set('cacher, 'retrouver, 'qualite, 'gris, 'espace, 'cacher_binaire, 'cacher_message, 'retrouver_binaire, 'retrouver_message)) {
      if (options.isDefinedAt(x)) {
        compte += 1
        if (compte > 1) {
          println("Vous ne pouvez pas combiner plusieurs commandes simultanément")
          sys.exit(1)
        }
      }
    }
    if (compte < 1) { // On vérifie qu'au moins une commande a été demandé
      println("Veuillez spécifier une commande")
      println(usage)
      sys.exit(1)
    }
    // On vérifie que les options sont associées aux bonnes commandes
    if (!options.isDefinedAt('qualite)) {
      for (x <- List('fenetre, 'alpha, 'beta, 'gamma, 'sigma, 'K1, 'K2, 'k)) {
        if (options.isDefinedAt(x)) {
          println("L'option --" + x.name + " ne peut être associée à une autre commande que qualite")
          sys.exit(1)
        }
      }
    }
    if (options.isDefinedAt('b64) && !(options.isDefinedAt('cacher_binaire) || options.isDefinedAt('cacher_message) || options.isDefinedAt('retrouver_binaire) || options.isDefinedAt('retrouver_message))) {
      println("L'option --Base64 ne peut être associée à une autre commande que cacher_binaire, cacher_message, retrouver_binaire ou retrouver_message")
      sys.exit(1)
    }
    // On vérifie l'existance des fichiers
    if (!Files.exists(Paths.get(options.getOrElse('ref, null).toString))) {
      println("Le fichier " + options.getOrElse('ref, null).toString + " n'existe pas")
      println("Veuillez corriger le chemin d'accès")
      sys.exit(1)
    }
    if ((options.isDefinedAt('cacher) || options.isDefinedAt('cacher_binaire)) && !Files.exists(Paths.get(options.getOrElse('cch, null).toString))) {
      println("Le fichier " + options.getOrElse('cch, null).toString + " n'existe pas")
      println("Veuillez corriger le chemin d'accès")
      sys.exit(1)
    }
    else if (options.isDefinedAt('qualite) && !Files.exists(Paths.get(options.getOrElse('mod, null).toString))) {
      println("Le fichier " + options.getOrElse('mod, null).toString + " n'existe pas")
      println("Veuillez corriger le chemin d'accès")
      sys.exit(1)
    }
    // On vérifie la bonne utilisation des arguments
    // cacher, retrouver, espace, cacher_binaire, cacher_message, retrouver_binaire ou retrouver_message
    val symSrcCch = List('rSrc, 'gSrc, 'bSrc, 'rCch, 'gCch, 'bCch)
    var srcCch:List[Int] = List()
    if (options.isDefinedAt('rSrc)) { // Si la liste d'argument a été spécifié
      val strSrcCch = symSrcCch.map{options.getOrElse(_, 3).toString}
      for (i <- 0 until symSrcCch.size) {
        if (!strSrcCch(i).isPositiveOrZeroInt) { // Int et >= 0
          println("L'argument <" + symSrcCch(i).name + "> est incorrecte: " + strSrcCch(i))
          println("Veuillez spécifier un entier entre 0 et 8")
          sys.exit(1)
        }
        srcCch = srcCch :+ strSrcCch(i).toInt
        if (srcCch(i) > 8) { // <= 8
          println("L'argument <" + symSrcCch(i).name + "> est incorrecte: " + strSrcCch(i))
          println("Veuillez spécifier un entier entre 0 et 8")
          sys.exit(1)
        }
      }
      // cacher ou retrouver
      if (options.isDefinedAt('rCch) && srcCch(0) + srcCch(1) + srcCch(2) != srcCch(3) + srcCch(4) + srcCch(5)) { // On vérifie que la répartition est correcte
        println("La répartition entre src et cch est incorrecte: " + (srcCch(0) + srcCch(1) + srcCch(2)) + " != " + (srcCch(3) + srcCch(4) + srcCch(5)))
        println("Veuillez spécifier des arguments tels que (<" + symSrcCch(0).name + "> + <" + symSrcCch(1).name + "> + <" + symSrcCch(2).name + ">) soit égal à (<" + symSrcCch(3).name + "> + <" + symSrcCch(4).name + "> + <" + symSrcCch(5).name + ">)")
        sys.exit(1)
      }
    }
    // qualite
    if (options.isDefinedAt('qualite)) {
      // fenetre
      if (options.isDefinedAt('fenetre)) {
        if (!options.getOrElse('fenetre, null).toString.isPositiveInt()) { // Int et > 0
          println("L'argument <fenetre> est incorrecte: " + options.getOrElse('fenetre, null).toString)
          println("Veuillez spécifier un entier naturel impair > 0")
          sys.exit(1)
        }
        val argFenetre = options.getOrElse('fenetre, null).toString.toInt
        if (argFenetre % 2 != 1) { // Impair
          println("L'argument <fenetre> est incorrecte: " + options.getOrElse('fenetre, null).toString)
          println("Veuillez spécifier un entier naturel impair > 0")
          sys.exit(1)
        }
      }
      // alpha, beta, gamma, sigma, k
      val symDoublePositive = List('alpha, 'beta, 'gamma, 'sigma, 'k)
      for (x <- symDoublePositive) {
        if (options.isDefinedAt(x)) {
          val tmpStr = options.getOrElse(x, null).toString
          if (!tmpStr.isPositiveDouble()) { // Double et > 0
            println("L'argument <" + x.name + "> est incorrecte: " + tmpStr)
            println("Veuillez spécifier un décimal > 0")
            sys.exit(1)
          }
        }
      }
      // K1, K2
      val symK = List('K1, 'K2)
      for (x <- symK) {
        if (options.isDefinedAt(x)) {
          val tmpStr = options.getOrElse(x, null).toString
          if (!tmpStr.isPositiveDouble() || tmpStr.toDouble > 0.1) { // Double, > 0 et <= 0.1
            println("L'argument <" + x.name + "> est incorrecte: " + tmpStr)
            println("Veuillez spécifier un décimal strictement positif <= 0.1")
            sys.exit(1)
          }
        }
      }
    }
    // On définit les valeurs par défaut des variables
    srcCch = if (srcCch == List()) List(3, 3, 3, 3, 3, 3) else srcCch
    val argFenetre = options.getOrElse('fenetre, 11).toString().toInt
    val argAlpha = options.getOrElse('alpha, 1.0).toString().toDouble
    val argBeta = options.getOrElse('beta, 1.0).toString().toDouble
    val argGamma = options.getOrElse('gamma, 1.0).toString().toDouble
    val argK1 = options.getOrElse('K1, 0.01).toString().toDouble
    val argK2 = options.getOrElse('K2, 0.03).toString().toDouble
    val argSigma = options.getOrElse('sigma, 1.5).toString().toDouble
    val argK = options.getOrElse('k, 45.0).toString().toDouble


    // Définitions
    // Image de référence
    var wrappedRef : ImageWrapper = new ImageWrapper(options.getOrElse('ref, null).toString) // RGB
    var imgRef : Array[Array[Int]] = wrappedRef.getImage()

    // Traitement des actions
    if (options.isDefinedAt('cacher)) {
      val hide = new HideHelper

      // Image à cacher
      var wrappedCch : ImageWrapper = new ImageWrapper(options.getOrElse('cch, null).toString) // RGB
      var imgCch : Array[Array[Int]] = wrappedCch.getImage()

      // On vérifie que la taille est identique
      if (imgRef.size != imgCch.size || imgRef(0).size != imgCch(0).size) {
        println("Les deux images n'ont pas les mêmes dimensions")
        sys.exit(1)
      }

      // On cache l'image et on sauvegarde
      hide.hide(imgRef, imgCch, srcCch)
      wrappedRef.saveImage(options.getOrElse('mod, null).toString)
    }
    else if (options.isDefinedAt('retrouver)) {
      val find = new FindHelper

      // On retrouve l'image et on sauvegarde
      find.find(imgRef, srcCch)
      wrappedRef.saveImage(options.getOrElse('cch, null).toString)
    }
    else if (options.isDefinedAt('qualite)) {
      val measure = new MeasureHelper
      val color = new ColorHelper

      // Image de référence
      var wrappedRefG : ImageWrapper = null // Niveaux de gris
      var imgRefG : Array[Array[Int]] = null

      // Image modifiée
      var wrappedMod : ImageWrapper = new ImageWrapper(options.getOrElse('mod, null).toString)
      var wrappedModG : ImageWrapper = null
      var imgMod : Array[Array[Int]] = wrappedMod.getImage()
      var imgModG : Array[Array[Int]] = null

      // On vérifie que la taille est identique
      if (imgRef.size != imgMod.size || imgRef(0).size != imgMod(0).size) {
        println("Les deux images n'ont pas les mêmes dimensions")
        sys.exit(1)
      }

      // Conversion de l'image de référence en niveau de gris
      wrappedRefG = new ImageWrapper(options.getOrElse('ref, null).toString)
      imgRefG = wrappedRefG.getImage()
      color.imgToGrayscale(imgRefG)
      var path = options.getOrElse('ref, null).toString
      path = path.patch(path.size - 4, "_g", 0)
      wrappedRefG.saveImage(path)
      wrappedRefG = new ImageWrapper(path) // On recharge l'image à cause de la compression JPEG
      imgRefG = wrappedRefG.getImage()

      // Conversion de l'image modifiée en niveau de gris
      wrappedModG = new ImageWrapper(options.getOrElse('mod, null).toString)
      imgModG = wrappedModG.getImage()
      color.imgToGrayscale(imgModG)
      path = options.getOrElse('mod, null).toString
      path = path.patch(path.size - 4, "_g", 0)
      wrappedModG.saveImage(path)
      wrappedModG = new ImageWrapper(path) // On recharge l'image à cause de la compression JPEG
      imgModG = wrappedModG.getImage()

      // Calcul des EQM
      val mse = measure.mse(imgRef, imgMod) // (RGB, R, G, B)
      val mseG = measure.mse_mono(imgRefG, imgModG) // GS

      // Calcul des métriques d'évaluation
      println("EQM = " + mse(0))
      println("Par couches: R = " + mse(1) + "; G = " + mse(2) + "; B = " + mse(3))
      println("En niveau de gris: GS = " + mseG)
      println()

      val snr = measure.snr(wrappedRef.getImage(), mse)
      println("SNR = " + snr(0) + " dB")
      println("Par couches: R = " + snr(1) + " dB; G = " + snr(2) + " dB; B = " + snr(3) + " dB")
      println("En niveau de gris: GS = " + measure.snr_mono(imgRefG, mseG) + " dB")
      println()

      val psnr = measure.psnr(mse)
      println("PSNR = " + psnr(0) + " dB")
      println("Par couches: R = " + psnr(1) + " dB; G = " + psnr(2) + " dB; B = " + psnr(3) + " dB")
      println("En niveau de gris: GS = " + measure.psnr_mono(mseG) + " dB")
      println("Un PSNR supérieur ou égal à 30 dB correspond généralement à une différence non perceptible")
      println()

      val ssim = measure.ssim(imgRefG, imgModG, argFenetre, argAlpha, argBeta, argGamma, argK1, argK2, argSigma) // On calcule la Map SSIM
      println("MSSIM = " + measure.meanOfArray(ssim))
      println()
      // On sauvegarde la Map SSIM
      val sav = options.getOrElse('mod, null).toString
      var wrappedSSIM : ImageWrapper = new ImageWrapper(sav)
      var imgSSIM = wrappedSSIM.getImage()
      color.doubleToImg(ssim, imgSSIM)
      wrappedSSIM.saveImage(sav.patch(sav.size - 4, "_ssim", 0))

      val dEab = measure.dEab(imgRef, imgMod) // On calcule la Map DeltaE*ab
      println("Moyenne dE*ab = " + measure.meanOfArray(dEab))
      println("Un dE*ab inférieur ou égal à environ 2,3 correspond généralement à une différence non perceptible")
      println()
      // On sauvegarde la Map Cr (plus cohérent pour comparaison avec la Map SSIM)
      val Cr = dEab.map(_.map(1 - _ / argK))
      var wrappeddEab : ImageWrapper = new ImageWrapper(sav)
      var imgdEab = wrappeddEab.getImage()
      color.doubleToImg(Cr, imgdEab) // On sauvegarde le négatif pondéré de la Map Cr(x, y)
      wrappeddEab.saveImage(sav.patch(sav.size - 4, "_dEab", 0))

      val cssim = measure.cssim(ssim, Cr) // On calcule la Map CSSIM
      println("MCSSIM = " + measure.meanOfArray(cssim))
      println()
      // On sauvegarde la Map CSSIM
      var wrappedCSSIM : ImageWrapper = new ImageWrapper(sav)
      var imgCSSIM = wrappedCSSIM.getImage()
      color.doubleToImg(cssim, imgCSSIM)
      wrappedCSSIM.saveImage(sav.patch(sav.size - 4, "_cssim", 0))
    }
    else if (options.isDefinedAt('gris)) {
      val color = new ColorHelper
      color.imgToGrayscale(imgRef)
      wrappedRef.saveImage(options.getOrElse('mod, null).toString)
    }
    else if (options.isDefinedAt('espace)) {
      val hide = new HideHelper
      val space = hide.space(imgRef, srcCch)
      println(space + " bits (" + space / 8.0 / 1024 + " Ko)")
    }
    else if (options.isDefinedAt('cacher_binaire)) {
      if (!options.getOrElse('mod, null).toString.endsWith(".png"))
        println("Attention: il est très fortement recommandé de sauvegarder au format PNG pour éviter la perte d'information!")
      val hide = new HideHelper

      // On cache le binaire et on sauvegarde l'image
      hide.hideBinary(imgRef, srcCch, Files.readAllBytes(Paths.get(options.getOrElse('cch, null).toString)), options.isDefinedAt('b64))
      wrappedRef.saveImage(options.getOrElse('mod, null).toString)
    }
    else if (options.isDefinedAt('cacher_message)) {
      if (!options.getOrElse('mod, null).toString.endsWith(".png"))
        println("Attention: il est très fortement recommandé de sauvegarder au format PNG pour éviter la perte d'information!")
      val hide = new HideHelper

      // On demande le message à l'utilisateur
      println("L'UTF-8 est supporté, il faut cependant mettre la variable d'environnement JAVA_OPTS=\"-Dfile.encoding=utf8\"")
      println("Veuillez entrer le message (pour valider, écrivez !FIN sur une nouvelle ligne suivi d'Entrée):")
      val msg = Iterator.continually(scala.io.StdIn.readLine).takeWhile(_ != "!FIN").mkString("\n")

      // On cache le message et on sauvegarde l'image
      hide.hideBinary(imgRef, srcCch, msg.getBytes(StandardCharsets.UTF_8), options.isDefinedAt('b64)) // On convertit le message en bytes encodés en UTF-8 (signés sur 8 bits)
      wrappedRef.saveImage(options.getOrElse('mod, null).toString)
    }
    else if (options.isDefinedAt('retrouver_binaire)) {
      val find = new FindHelper

      // On retrouve le binaire et on le sauvegarde
      Files.write(Paths.get(options.getOrElse('cch, null).toString), find.findBinary(imgRef, srcCch, options.isDefinedAt('b64)))
    }
    else if (options.isDefinedAt('retrouver_message)) {
      val find = new FindHelper

      // On retrouve le message
      val str = new String(find.findBinary(imgRef, srcCch, options.isDefinedAt('b64)), StandardCharsets.UTF_8)
      println("Voici le message caché:")
      println(str)
    }
  }
}
