package helper

import helper.ColorHelper
import math.{pow, log10, floor, Pi, exp, min, max, round, sqrt}


class MeasureHelper {
  /**
   * Ensemble des fonctions ou méthodes permettant d’effectuer les mesures de qualité d’image
   */
  
  val color = new ColorHelper

  // Fonctions de mesures pour les images RGB
  def mse(ref:Array[Array[Int]], mod:Array[Array[Int]]): Array[Double] = {
    /**
     * Renvoie les mesures de l'EQM entre deux images couleurs
     * 
     * @author Florent Caspar
     * 
     * @param ref
     *              l'image de référence sous forme d'un tableau d'entiers
     * @param mod
     *              l'image modifiée sous forme d'un tableau d'entiers
     * 
     * @return La mesure de l'EQM globale et pour chaque couche sous forme d'un tableau de décimaux [Global, Rouge, Vert, Bleu]
     */
    var sumR:Double = 0
    var sumG:Double = 0
    var sumB:Double = 0
    val nbPix:Double = ref.size * ref(0).size
    for (j <- 0 to ref.size - 1) { // Pour chaque pixel
      for (i <- 0 to ref(0).size - 1) {
        sumR += pow(color.getRed(ref(j)(i)) - color.getRed(mod(j)(i)), 2) // On somme la différence d'intensité au carrée sur chaque couche RGB
        sumG += pow(color.getGreen(ref(j)(i)) - color.getGreen(mod(j)(i)), 2)
        sumB += pow(color.getBlue(ref(j)(i)) - color.getBlue(mod(j)(i)), 2)
      }
    }
    sumR /= nbPix
    sumG /= nbPix
    sumB /= nbPix
    return Array((sumR + sumG + sumB) / 3, sumR, sumG, sumB) // On retourne l'EQM globale et sur chacune des couches
  }

  def snr(ref:Array[Array[Int]], mse:Array[Double]): Array[Double] = {
    /**
     * Renvoie les SNR entre deux images couleurs
     * 
     * @author Florent Caspar
     * 
     * @param ref
     *              l'image de référence sous forme d'un tableau d'entiers
     * @param mse
     *              mesure de l'EQM globale et pour chaque couche sous forme d'un tableau de décimaux [Global, Rouge, Vert, Bleu]
     * 
     * @return Le SNR global et pour chaque couche sous forme d'un tableau de décimaux [Global, Rouge, Vert, Bleu]
     */
    var sumR:Double = 0
    var sumG:Double = 0
    var sumB:Double = 0
    for (j <- 0 to ref.size - 1) { // Pour chaque pixel
      for (i <- 0 to ref(0).size - 1) {
        sumR += pow(color.getRed(ref(j)(i)), 2) // On somme l'intensité au carrée sur chaque couche RGB
        sumG += pow(color.getGreen(ref(j)(i)), 2)
        sumB += pow(color.getBlue(ref(j)(i)), 2)
      }
    }
    val cst = -10 * log10(ref.size * ref(0).size)
    return Array(10 * log10((sumR + sumG + sumB) / (mse(0) * 3)) + cst, 10 * log10(sumR / mse(1)) + cst, 10 * log10(sumG / mse(2)) + cst, 10 * log10(sumB / mse(3)) + cst)
  }

  def psnr(mse:Array[Double]): Array[Double] = {
    /**
     * Renvoie les PSNR entre deux images couleurs
     * 
     * @author Florent Caspar
     * 
     * @param mse
     *              mesure de l'EQM globale et pour chaque couche sous forme d'un tableau de décimaux [Global, Rouge, Vert, Bleu]
     * 
     * @return Le PSNR global et pour chaque couche sous forme d'un tableau de décimaux [Global, Rouge, Vert, Bleu]
     */
    val cst = 20 * log10(255)
    return Array(cst - 10 * log10(mse(0)), cst - 10 * log10(mse(1)), cst - 10 * log10(mse(2)), cst - 10 * log10(mse(3)))
  }


  // Fonctions de mesures pour les images monocouches (i.e. en niveau de gris)
  def mse_mono(ref:Array[Array[Int]], mod:Array[Array[Int]]): Double = {
    /**
     * Renvoie la mesure de l'EQM entre deux images en niveau de gris
     * 
     * @author Florent Caspar
     * 
     * @param ref
     *              l'image de référence en niveau de gris sous forme d'un tableau d'entiers
     * @param mod
     *              l'image modifiée en niveau de gris sous forme d'un tableau d'entiers
     * 
     * @return La mesure de l'EQM sous forme d'un décimal
     */
    var sum:Double = 0
    val nbPix:Double = ref.size * ref(0).size
    for (j <- 0 to ref.size - 1) { // Pour chaque pixel
      for (i <- 0 to ref(0).size - 1) {
        sum += pow(color.getBlue(ref(j)(i)) - color.getBlue(mod(j)(i)), 2) // On somme la différence d'intensité au carrée sur une des couches (car R = G = B)
      }
    }
    sum /= nbPix
    return sum // On retourne l'EQM de la couche
  }

  def snr_mono(ref:Array[Array[Int]], mse:Double): Double = {
    /**
     * Renvoie le SNR entre deux images en niveau de gris
     * 
     * @author Florent Caspar
     * 
     * @param ref
     *              l'image de référence en niveau de gris sous forme d'un tableau d'entiers
     * @param mse
     *              mesure de l'EQM du niveau de gris sous forme d'un décimal
     * 
     * @return Le SNR sous forme d'un décimal
     */
    var sum:Double = 0
    for (j <- 0 until ref.size) { // Pour chaque pixel
      for (i <- 0 until ref(0).size) {
        sum += pow(color.getBlue(ref(j)(i)), 2) // On somme l'intensité au carrée sur une des couches (car R = G = B)
      }
    }
    return 10 * log10(sum / (mse * ref.size * ref(0).size))
  }

  def psnr_mono(mse:Double): Double = {
    /**
     * Renvoie le PSNR entre deux images en niveau de gris
     * 
     * @author Florent Caspar
     * 
     * @param mse
     *              mesure de l'EQM du niveau de gris sous forme d'un décimal
     * 
     * @return Le PSNR sous forme d'un décimal
     */
    return 20 * log10(255) - 10 * log10(mse)
  }

  def ssim(refG:Array[Array[Int]], modG:Array[Array[Int]], N:Int, alpha:Double, beta:Double, gamma:Double, K1:Double, K2:Double, sigma:Double): Array[Array[Double]] = {
    /**
     * Renvoie un tableau de l'ensemble des valeurs de la SSIM pour chaque pixel
     * 
     * @author Florent Caspar
     * 
     * @param refG
     *              l'image de référence en niveau de gris sous forme d'un tableau d'entiers
     * @param modG
     *              l'image modifiée en niveau de gris sous forme d'un tableau d'entiers
     * @param N
     *              taille des fenêtres pour le SSIM sous forme d'entier
     * @param alpha
     *              paramètre d'ajustement de la luminance sous forme d'un décimal
     * @param beta
     *              paramètre d'ajustement du contraste sous forme d'un décimal
     * @param gamma
     *              paramètre d'ajustement de la structure sous forme d'un décimal
     * @param K1
     *              constante du calcul de la SSIM sous forme d'un décimal
     * @param K2
     *              constante du calcul de la SSIM sous forme d'un décimal
     * @param sigma
     *              ecart-type de la distribution gaussienne normalisée appliquée sur chaque fenêtre sous forme d'un décimal
     * 
     * @return L'ensemble des valeurs de la SSIM pour chaque pixel sous forme d'un tableau de décimaux
     */
    val sY = refG.size
    val sX = refG(0).size
    if (sX < N || sY < N) return null
    val c1 = pow(K1 * 255, 2)
    val c2 = pow(K2 * 255, 2)
    val c3 = c2 / 2.0

    // On génère la distribution gaussienne normalisée d'écart-type sigma
    var w:Array[Array[Double]] = Array.ofDim(N, N)
    val dN = floor(N / 2).toInt
    val expDenom = 2 * pow(sigma, 2)
    val denom = expDenom * Pi
    var sumW:Double = 0.0 // == 1
    for (j <- -dN to dN; i <- -dN to dN) {
      w(dN + j)(dN + i) = exp(-(pow(i, 2) + pow(j, 2)) / expDenom) / denom
      sumW += w(dN + j)(dN + i)
    }
    w = w.map{_.map{_ / sumW}} // On normalise

    var refD:Array[Array[Double]] = refG.map{_.map{color.getBlue(_).toDouble}} // On prend une seule composante RGB des niveaux de gris
    var modD:Array[Array[Double]] = modG.map{_.map{color.getBlue(_).toDouble}}

    var ssimMap:Array[Array[Double]] = Array.ofDim(sY, sX)
    for (y <- 0 until sY; x <- 0 until sX) {
      // On calcule la moyenne, la variance et la covariance sur chaque fenêtre depuis chaque pixel
      var meanX:Double = 0.0
      var meanY:Double = 0.0
      for (j <- y - dN to y + dN; i <- x - dN to x + dN) {
        val rj = max(0, min(j, sY - 1)); val ri = max(0, min(i, sX - 1)) // On réplique les valeurs en bordures en cas de dépassement
        meanX += w(j - y + dN)(i - x + dN) * refD(rj)(ri)
        meanY += w(j - y + dN)(i - x + dN) * modD(rj)(ri)
      }
      //meanX /= n // Inutile de diviser par n car on applique un filtre gaussien
      //meanY /= n
      var varX:Double = 0.0
      var varY:Double = 0.0
      var covXY: Double = 0.0
      for (j <- y - dN to y + dN; i <- x - dN to x + dN) {
        val rj = max(0, min(j, sY - 1)); val ri = max(0, min(i, sX - 1)) // On réplique les valeurs en bordures en cas de dépassement
        val wi = w(j - y + dN)(i - x + dN)
        val vX = refD(rj)(ri) - meanX
        val vY = modD(rj)(ri) - meanY
        varX += wi * pow(vX, 2)
        varY += wi * pow(vY, 2)
        covXY += wi * vX * vY
      }
      val stdX = sqrt(varX)
      val stdY = sqrt(varY)
      //varX /= (n - 1)
      //varY /= (n - 1)
      //covXY /= (n - 1)

      // Formule générale
      ssimMap(y)(x) = pow((2 * meanX * meanY + c1) / (meanX * meanX + meanY * meanY + c1), alpha) * // l(x,y)^alpha
                      pow((2 * stdX * stdY + c2) / (varX + varY + c2), beta) * // c(x,y)^beta
                      pow(((covXY + c3) / (stdX * stdY + c3)), gamma) // s(x,y)^gamma
      //ssimMap(y)(x) = (2 * meanX * meanY + c1) * (2 * covXY + c2) / ((meanX * meanX + meanY * meanY + c1) * (varX + varY + c2)) // Formule simplifié
    }
    return ssimMap
  }

  def cssim(ssim:Array[Array[Double]], Cr:Array[Array[Double]]): Array[Array[Double]] = {
    /**
     * Renvoie un tableau de l'ensemble des valeurs de la CSSIM pour chaque pixel
     * 
     * @author Florent Caspar
     * 
     * @param ssim
     *              ensemble des SSIM entre deux images sous forme d'un tableau de décimaux
     * @param Cr
     *              ensemble des Cr entre les deux images sous forme d'un tableau de décimaux
     * 
     * @return L'ensemble des valeurs de la CSSIM pour chaque pixel sous forme d'un tableau de décimaux
     */
    // Multiplication élément par élément entre les deux Array
    var c:Array[Array[Double]] = Array.ofDim(ssim.size, ssim(0).size)
    for (j <- 0 until c.size; i <- 0 until c(0).size) {
      c(j)(i) = ssim(j)(i) * Cr(j)(i)
    }
    return c
  }

  def meanOfArray(arr:Array[Array[Double]]): Double = {
    /**
     * Renvoie la moyenne des éléments d'un tableau bidimensionnel
     * 
     * @author Florent Caspar
     * 
     * @param arr
     *              un tableau bidimensionnel
     * 
     * @return La moyenne sous forme d'un décimal
     */
    var sum:Double = 0.0
    for (y <- arr; x <- y) {
      sum += x
    }
    sum /= (arr.size * arr(0).size)
    return sum
  }

  def dEab(ref:Array[Array[Int]], mod:Array[Array[Int]]): Array[Array[Double]] = {
    /**
     * Renvoie un tableau de l'ensemble des valeurs du deltaE*ab entre deux images et pour chaque pixel
     * 
     * @author Florent Caspar
     * 
     * @param ref
     *              l'image de référence sous forme d'un tableau d'entiers
     * @param mod
     *              l'image modifiée sous forme d'un tableau d'entiers
     * 
     * @return L'ensemble des valeurs du deltaE*ab pour chaque pixel sous forme d'un tableau de décimaux
     */
    var dE:Array[Array[Double]] = Array.ofDim(ref.size, ref(0).size)
    val Lab1 = color.imgCIELab(ref)
    val Lab2 = color.imgCIELab(mod)
    for (j <- 0 until Lab1.size; i <- 0 until Lab1(0).size) {
      dE(j)(i) = pow(pow(Lab1(j)(i)(0) - Lab2(j)(i)(0), 2)
                      + pow(Lab1(j)(i)(1) - Lab2(j)(i)(1), 2)
                      + pow(Lab1(j)(i)(2) - Lab2(j)(i)(2), 2), 0.5)
    }
    return dE
  }
}