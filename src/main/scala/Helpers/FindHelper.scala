package helper

import java.util.Base64


class FindHelper {
  /**
   * Ensemble des fonctions ou méthodes permettant de retrouver l’information depuis une image
   */

  def find(imgA: Array[Array[Int]], srcCch: List[Int]) {
    /**
     * Permet de retrouver l'image cachée avec la méthode LSB (en "écrasant" l'image)
     * 
     * @author Kamal Allouche, revu par Florent Caspar
     * 
     * @param imgA
     *              l'image qui contient une image cachée
     * @param srcCch
     *              liste des nombres de LSB (resp. MSB) sur chaque couche de l'image A (resp. cachée)
     */
    // srcCch = List(rSrc, gSrc, bSrc, rCch, gCch, bCch)
    // idx            0     1     2     3     4     5
    for (row <- 0 until imgA.size) {
      for (col <- 0 until imgA(0).size) {

        var A: String = imgA(row)(col).toBinaryString  // From Int to String
        val ACch = A.substring(16-srcCch(0),16) + A.substring(24-srcCch(1),24) + A.substring(32-srcCch(2),32) // On extrait la totalité de l'information cachée
        // ACch = les rSrc, gSrc et bSrc LSB (concaténés) des couches respectives r, g et b de l'image modifiée

        A = List.fill(8)("1").mkString + // On ne peut pas cacher d'informations dans l'alpha car ImageWrapper la met à 0xFF en sauvegardant (peu importe sa valeur)
            ACch.substring(0,srcCch(3)) + List.fill(8-srcCch(3))("0").mkString + // rCch des MSB de l'image cachée + 8-rCch "0"
            ACch.substring(srcCch(3),srcCch(3)+srcCch(4)) + List.fill(8-srcCch(4))("0").mkString +
            ACch.substring(srcCch(3)+srcCch(4),srcCch(3)+srcCch(4)+srcCch(5)) + List.fill(8-srcCch(5))("0").mkString
        
        imgA(row)(col) = Integer.parseUnsignedInt(A, 2) // From String to Int
        
      }
    }
  }

  def findBinary(image: Array[Array[Int]], lsb: List[Int], b64: Boolean): Array[Byte] = {
    /**
     * Renvoie le binaire caché dans l'image avec la méthode LSB
     * 
     * @author Adonis Kindo, revu par Florent Caspar
     * 
     * @param image
     *              l'image où le binaire a été caché sous forme d'un tableau d'entiers
     * @param lsb
     *              liste des nombres de LSB sur chaque couche de l'image
     * @param b64
     *              vrai si le décodage Base64 doit être utilisé
     * 
     * @return Le binaire caché sous forme d'un tableau d'octets
     */
    // lsb = List(rLsb, gLsb, bLsb)
    // idx         0     1     2
    var hiddenBuilder = new StringBuilder() // La boucle est horriblement longue avec des String simples

    for (row <- 0 until image.size) {
        for (col <-0 until image(0).size) {
    
            val A: String = image(row)(col).toBinaryString

            hiddenBuilder.append(A.substring(16-lsb(0),16) + A.substring(24-lsb(1),24) + A.substring(32-lsb(2),32)) // On extrait la totalité de l'information cachée
            
        }
    }
    val hidden = hiddenBuilder.toString()

    var idx = 64 // On a besoin d'un indice pour parcourir l'ensemble de l'info
    var bin:Array[Byte] = Array()
    var last = Integer.parseInt(hidden.substring(idx,idx+8), 2).toByte
    val size = Integer.parseInt(hidden.substring(0,64), 2) // On récupère la taille totale du binaire (avec les 64 bits d'entête)
    while (idx < size) { // Tant qu'on a pas atteint la taille totale du binaire
      bin = bin :+ last
      idx += 8
      last = Integer.parseInt(hidden.substring(idx,idx+8), 2).toByte
    }
    if (b64) {
      bin = Base64.getMimeDecoder().decode(bin) // On décode le Base64
    }
    return bin
  }

}
