package helper

import java.util.Base64


class HideHelper {
  /**
   * Ensemble des fonctions ou méthodes permettant de cacher l’information dans une image
   */
  
  def hide(imgA: Array[Array[Int]], imgB: Array[Array[Int]], srcCch: List[Int]) {
    /**
     * Modifie l'image A pour y cacher l'image B avec la méthode LSB
     * 
     * @author Kamal Allouche, revu par Florent Caspar
     * 
     * @param imgA
     *              l'image où l'autre image va être cachée sous forme d'un tableau d'entiers
     * @param imgB
     *              l'image à cacher sous forme d'un tableau d'entiers
     * @param srcCch
     *              liste des nombres de LSB (resp. MSB) sur chaque couche de l'image A (resp. B)
     */
    // srcCch = List(rSrc, gSrc, bSrc, rCch, gCch, bCch)
    // idx            0     1     2     3     4     5
    // type          LSB   LSB   LSB   MSB   MSB   MSB
    for (row <- 0 until imgA.size) {
      for (col <- 0 until imgA(0).size) {

        var A: String = imgA(row)(col).toBinaryString  // From Int to String
        var BCch: String = imgB(row)(col).toBinaryString  // From Int to String

        BCch = BCch.substring(8,8+srcCch(3)) + BCch.substring(16,16+srcCch(4)) + BCch.substring(24,24+srcCch(5)) // On extrait la totalité de l'information à cacher
        // BCch = les rCch, gCch et bCch MSB (concaténés) des couches respectives r, g et b de l'image à cacher

        A = List.fill(8)("1").mkString + // On ne peut pas cacher d'informations dans l'alpha car ImageWrapper la met à 0xFF en sauvegardant (peu importe sa valeur)
            A.substring(8,16-srcCch(0)) + BCch.substring(0,srcCch(0)) + // 8 - rLSB de l'original + rLSB de BCch
            A.substring(16,24-srcCch(1)) + BCch.substring(srcCch(0),srcCch(0)+srcCch(1)) +
            A.substring(24,32-srcCch(2)) + BCch.substring(srcCch(0)+srcCch(1),srcCch(0)+srcCch(1)+srcCch(2))
        
        imgA(row)(col) = Integer.parseUnsignedInt(A, 2) // From String to Int
      }
    }
  
  }

  def space(img: Array[Array[Int]], lsb: List[Int]): Int = {
    /**
     * Renvoie l'espace disponible dans l'image, en bits, avec la méthode LSB
     * 
     * @author Adonis Kindo
     * 
     * @param img
     *              l'image sous forme d'un tableau d'entiers
     * @param lsb
     *              liste des nombres de LSB sur chaque couche de l'image
     * 
     * @return L'espace disponible dans l'image, en bits, sous forme d'un entier
     */
    // lsb = List(rLsb, gLsb, bLsb)
    // idx         0     1     2
    // Valeurs maximales des métriques avec lsb par défaut:
    // EQM: 49.0 ; Par couches: R=49.0 , G=49.0 , B=49.0 , Gs=36.0
    // SNR: 31.228842 ; Par couches: R=31.228842 , G=32.533649 , B=31.228842, Gs=31.228842
    // PSNR: 31.228842 ; Par couches: R=31.228842 , G=31.228842 ,B=31.228842, Gs=32.567778
    // MSSIM: 0.9997143
    // De*AB : 2.4210981
    // MCSSIM: 0.9459275
    return (lsb(0) + lsb(1) + lsb(2)) * img.size * img(0).size
  }

  def hideBinary(image: Array[Array[Int]], lsb: List[Int], bin: Array[Byte], b64: Boolean) {
    /**
     * Modifie l'image pour y cacher un binaire avec la méthode LSB
     * 
     * @author Adonis Kindo, revu par Florent Caspar
     * 
     * @param image
     *              l'image, où le binaire va être cachée, sous forme d'un tableau d'entiers
     * @param lsb
     *              liste des nombres de LSB sur chaque couche de l'image
     * @param bin
     *              le binaire à cacher sous forme d'un tableau d'octets
     * @param b64
     *              vrai si l'encodage Base64 doit être utilisé
     */
    // lsb = List(rLsb, gLsb, bLsb)
    // idx         0     1     2
    var byt:Array[Byte] = bin
    if (b64) { // Si en Base64, on encode encore les bytes
      byt = Base64.getMimeEncoder().encode(bin)
    }
    var binary = byt.map({x => Integer.toBinaryString((x & 0xFF) + 0x100).substring(1)}).mkString(""); // Pour convertir les bytes en String sur 8 bits: on fait un ET logique pour n'avoir que le dernier octet, on ajoute 0x100 pour rajouter les 0 nécessaires à l'octet, on convertit en String et on supprime le premier caractère qui ne servait qu'à rajouter les 0
    val binarySize = (64 + binary.size).toBinaryString // On se réserve 64 bits pour stocker la longueur du binaire
    binary = List.fill(64 - binarySize.size)("0").mkString + binarySize + binary // On ajoute la longueur du binaire en entête du binaire (dans les 64 premiers bits)
    val totalSpace = space(image, lsb)
    val usedSpace = binary.size
    if (usedSpace > totalSpace) {
      println("L'espace demandé (" + usedSpace + " bits) est supérieur à celui disponible (" + totalSpace + " bits)")
      sys.exit(1)
    }
    binary += List.fill(lsb(0)+lsb(1)+lsb(2))("0").mkString // On remplit de 0 les éventuels bits restants de la dernière itération
    println("Espace utilisé: " + usedSpace + " / " + totalSpace + " bits (" + usedSpace / 8.0 / 1024 + " / " + totalSpace / 8.0 / 1024 + " Ko)")
    var idx = 0 // On a besoin d'un indice pour parcourir l'ensemble du texte

    for (row <- 0 until image.size) {
        for (col <-0 until image(0).size) {
    
            var A: String = image(row)(col).toBinaryString

            if (idx < usedSpace) { // Tant qu'il reste de l'information à cacher
              A = List.fill(8)("1").mkString + // On ne peut pas cacher d'informations dans l'alpha car ImageWrapper la met à 0xFF en sauvegardant (peu importe sa valeur)
                  A.substring(8,16-lsb(0)) + binary.substring(idx,idx+lsb(0)) + // 8 - rLSB de l'original + rLSB de binary
                  A.substring(16,24-lsb(1)) + binary.substring(idx+lsb(0),idx+lsb(0)+lsb(1)) +
                  A.substring(24,32-lsb(2)) + binary.substring(idx+lsb(0)+lsb(1),idx+lsb(0)+lsb(1)+lsb(2))
              idx += lsb(0)+lsb(1)+lsb(2)
            }

            image(row)(col) = Integer.parseUnsignedInt(A, 2)
            
        }
    }
  }

}