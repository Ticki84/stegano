package helper

import math.{pow, round, min, max}


class ColorHelper {
  /**
   * Ensemble des fonctions ou méthodes permettant de convertir une image, une couleur ou une partie d’une couleur
   * dans un autre espace ou dans une autre unité
   */

  def getRed(color:Int): Int = {
    /**
     * Retourne l'intensité de la couche rouge
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité de la couche rouge sous forme d'un entier compris entre 0 et 255
     */
    return ((color & 0x00ff0000) >> 16).toInt // On isole l'octet de la couche rouge en mettant les autres octets à 0x00 et en faisant un shift right de 16 bits
  }
  def getGreen(color:Int): Int = {
    /**
     * Retourne l'intensité de la couche verte
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité de la couche verte sous forme d'un entier compris entre 0 et 255
     */
    return ((color & 0x0000ff00) >> 8).toInt
  }
  def getBlue(color:Int): Int = {
    /**
     * Retourne l'intensité de la couche bleue
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité de la couche bleue sous forme d'un entier compris entre 0 et 255
     */
    return ((color & 0x000000ff)     ).toInt
  }

  def getNormRed(color:Int): Double = {
    /**
     * Retourne l'intensité normalisée de la couche rouge
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité normalisée de la couche rouge sous forme d'un décimal compris entre 0 et 1
     */
    return (((color & 0x00ff0000) >> 16)/255.0).toDouble
  }
  def getNormGreen(color:Int): Double = {
    /**
     * Retourne l'intensité normalisée de la couche verte
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité normalisée de la couche verte sous forme d'un décimal compris entre 0 et 1
     */
    return (((color & 0x0000ff00) >> 8)/255.0).toDouble
  }
  def getNormBlue(color:Int): Double = {
    /**
     * Retourne l'intensité normalisée de la couche bleue
     * 
     * @author Florent Caspar
     * 
     * @param color
     *            la couleur sous forme d'un entier
     * 
     * @return l'intensité normalisée de la couche bleue sous forme d'un décimal compris entre 0 et 1
     */
    return (((color & 0x000000ff)     )/255.0).toDouble
  }

  def imgToGrayscale(img:Array[Array[Int]]) = {
    /**
     * Convertit l'image passée en argument en niveau de gris
     * 
     * @author Florent Caspar
     * 
     * @param img
     *            l'image sous forme d'un tableau d'entiers
     */
    for (j <- 0 until img.size) { // Pour chaque pixel
      for (i <- 0 until img(0).size) {
        val color = img(j)(i)
        var R = getNormRed(color)
        var G = getNormGreen(color)
        var B = getNormBlue(color)
        // Transformation en valeurs linéaires
        if (R > 0.04045) R = pow(((R + 0.055) / 1.055), 2.4)
        else R /= 12.92
        if ( G > 0.04045 ) G = pow(((G + 0.055) / 1.055), 2.4)
        else G /= 12.92
        if (B > 0.04045) B = pow(((B + 0.055) / 1.055), 2.4)
        else B /= 12.92
        // Calcul de la luminance
        val L = 0.2126 * R + 0.7152 * G + 0.0722 * B
        // Conversion en RGB (R = G = B)
        var RGB:Int = 0
        if (L > 0.0031308) RGB = ((1.055 * pow(L, 1/2.4) - 0.055)*255).toInt
        else RGB = (12.92 * L * 255).toInt
        val s = RGB.toHexString.toUpperCase
        // Conversion en entier
        img(j)(i) = Integer.parseInt(String.format("00%2s%2s%2s", s, s, s).replace(' ','0'), 16)
      }
    }
  }

  def doubleToImg(v:Array[Array[Double]], img:Array[Array[Int]]) = {
    /**
     * Convertit un ensemble de décimaux en intensité de niveau de gris et les place sur l'image passée en argument
     * 
     * @author Florent Caspar
     * 
     * @param v
     *            l'ensemble de décimaux (comprises entre 0 et 1) sous forme d'un tableau de décimaux
     * @param img
     *            l'image sous forme d'un tableau d'entiers
     */
    for (j <- 0 until v.size; i <- 0 until v(0).size) {
      val s = min((max(v(j)(i), 0) * 255), 255).toInt.toHexString.toUpperCase
      img(j)(i) = Integer.parseInt(String.format("00%2s%2s%2s", s, s, s).replace(' ','0'), 16)
    }
  }

  def downsample(img:Array[Array[Int]], factor:Int): Array[Array[Int]] = {
    /**
     * Permet de sous-échantillonnée une image couleur par un facteur entier
     * 
     * @author Florent Caspar
     * 
     * @param img
     *            l'image sous forme d'un tableau d'entiers
     * @param factor
     *            le facteur entier de diminution
     * 
     * @return l'image sous-échantillonnée sous forme d'un tableau d'entiers
     * 
     * @deprecated Un facteur entier est trop restrictif
     */
    var nImg:Array[Array[Int]] = Array.ofDim(img.size / factor, img(0).size / factor)
    for (j <- 0 until nImg.size; i <- 0 until nImg(0).size) {
      var R = 0
      var G = 0
      var B = 0
      for (y <- j until j + factor; x <- i until i + factor) {
        R += getRed(img(y)(x))
        G += getGreen(img(y)(x))
        B += getBlue(img(y)(x))
      }
      R = (R * pow(factor, -2)).toInt // On fait la moyenne d'intensité sur factor*factor pixels
      G = (G * pow(factor, -2)).toInt
      B = (B * pow(factor, -2)).toInt
      val s = nImg(j)(i).toHexString.toUpperCase
      // Conversion en entier
      nImg(j)(i) = Integer.parseInt(String.format("00%2s%2s%2s",
        R.toHexString.toUpperCase,
        G.toHexString.toUpperCase,
        B.toHexString.toUpperCase).replace(' ','0'), 16)
    }
    return nImg
  }

  def downsample_mono(img:Array[Array[Int]], factor:Int): Array[Array[Int]] = {
    /**
     * Permet de sous-échantillonnée une image en niveau de gris par un facteur entier
     * 
     * @author Florent Caspar
     * 
     * @param img
     *            l'image en niveau de gris sous forme d'un tableau d'entiers
     * @param factor
     *            le facteur entier de diminution
     * 
     * @return l'image sous-échantillonnée sous forme d'un tableau d'entiers
     * 
     * @deprecated Un facteur entier est trop restrictif
     */
    var nImg:Array[Array[Int]] = Array.ofDim(img.size / factor, img(0).size / factor)
    for (j <- 0 until nImg.size; i <- 0 until nImg(0).size) {
      nImg(j)(i) = (img(j)(i) * pow(factor, -2)).toInt // On fait la moyenne d'intensité sur factor*factor pixels
      val s = nImg(j)(i).toHexString.toUpperCase
      // Conversion en entier
      nImg(j)(i) = Integer.parseInt(String.format("00%2s%2s%2s", s, s, s).replace(' ','0'), 16)
    }
    return nImg
  }

  def imgCIELab(img:Array[Array[Int]]): Array[Array[Array[Double]]] = {
    /**
     * Renvoie l'image convertit de l'espace RGB à CIEL*ab
     * 
     * @author Florent Caspar
     * 
     * @param img
     *            l'image sous forme d'un tableau d'entiers
     * 
     * @return l'image dans l'espace CIEL*ab sous forme d'un tableau tridimensionnel de décimaux
     */
    var CIELab:Array[Array[Array[Double]]] = Array.ofDim(img.size, img(0).size, 3)
    val add:Double = 16.0 / 116
    val power:Double = 1.0/3

    for (j <- 0 until img.size; i <- 0 until img(0).size) {
      // RGB -> XYZ
      val color = img(j)(i)
      var R = getNormRed(color)
      var G = getNormGreen(color)
      var B = getNormBlue(color)
      // Transformation en valeurs linéaires
      if (R > 0.04045) R = pow(((R + 0.055) / 1.055), 2.4)
      else R /= 12.92
      if ( G > 0.04045 ) G = pow(((G + 0.055) / 1.055), 2.4)
      else G /= 12.92
      if (B > 0.04045) B = pow(((B + 0.055) / 1.055), 2.4)
      else B /= 12.92
      var X = R * 41.24 + G * 35.76 + B * 18.05
      var Y = R * 21.26 + G * 71.52 + B * 7.22
      var Z = R * 1.93 + G * 11.92 + B * 95.05
      // ~RGB -> XYZ
      // XYZ -> CIEL*ab
      X /= 94.811; Y /= 100.0; Z /= 107.304
      if (X > 0.008856) X = pow(X, power)
      else X = (7.787 * X) + add
      if (Y > 0.008856) Y = pow(Y, power)
      else Y = (7.787 * Y) + add
      if (Z > 0.008856) Z = pow(Z, power)
      else Z = (7.787 * Z) + add
      CIELab(j)(i)(0) = (116 * Y) - 16 // L*
      CIELab(j)(i)(1) = 500 * (X - Y) // a*
      CIELab(j)(i)(2) = 200 * (Y - Z) // b*
      // ~XYZ -> CIEL*ab
    }
    return CIELab
  }
}
