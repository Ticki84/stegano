if [[ $# == 7 ]] ; then
	cha=9
	n=1
	i=$5
	j=$6
	k=$7
	while [[ $i -ge 4 ]]
	do
		while [[ $j -ge 4 ]]
		do
			while [[ $k -ge 4 ]]
			do
				eval "java -jar stegano.jar cacher $1 $2 $3 $i $j $k $i $j $k"
                eval "java -jar stegano.jar retrouver $3 $4 $i $j $k $i $j $k"
				out=$(eval "java -jar stegano.jar qualite $2 $4")
				oeqm=$(echo "$out" | head -n 3)
				osnr=$(echo "$out" | head -n 7 | tail -n 3)
				opsnr=$(echo "$out" | head -n 11 | tail -n 3)
				read -r eqm reqm geqm beqm gseqm <<< $(echo "$oeqm" | gawk '
				match($0, /EQM = (.+)$/, m) {RGB=m[1]}
				match($0, /R = (.+); G = (.+); B = (.+)$/, m) {R=m[1]; G=m[2]; B=m[3]}
				match($0, /GS = (.+)$/, m) {GS=m[1]}
				END {print RGB, R, G, B, GS;}')
				eqm=$(echo "$eqm" | cut -c -$cha)
				reqm=$(echo "$reqm" | cut -c -$cha)
				geqm=$(echo "$geqm" | cut -c -$cha)
				beqm=$(echo "$beqm" | cut -c -$cha)
				gseqm=$(echo "$gseqm" | cut -c -$cha)
				read -r snr rsnr gsnr bsnr gssnr <<< $(echo "$osnr" | gawk '
				match($0, /SNR = (.+) dB$/, m) {RGB=m[1]}
				match($0, /R = (.+) dB; G = (.+) dB; B = (.+) dB$/, m) {R=m[1]; G=m[2]; B=m[3]}
				match($0, /GS = (.+) dB$/, m) {GS=m[1]}
				END {print RGB, R, G, B, GS;}')
				snr=$(echo "$snr" | cut -c -$cha)
				rsnr=$(echo "$rsnr" | cut -c -$cha)
				gsnr=$(echo "$gsnr" | cut -c -$cha)
				bsnr=$(echo "$bsnr" | cut -c -$cha)
				gssnr=$(echo "$gssnr" | cut -c -$cha)
				read -r psnr rpsnr gpsnr bpsnr gspsnr <<< $(echo "$opsnr" | gawk '
				match($0, /PSNR = (.+) dB$/, m) {RGB=m[1]}
				match($0, /R = (.+) dB; G = (.+) dB; B = (.+) dB$/, m) {R=m[1]; G=m[2]; B=m[3]}
				match($0, /GS = (.+) dB$/, m) {GS=m[1]}
				END {print RGB, R, G, B, GS;}')
				psnr=$(echo "$psnr" | cut -c -$cha)
				rpsnr=$(echo "$rpsnr" | cut -c -$cha)
				gpsnr=$(echo "$gpsnr" | cut -c -$cha)
				bpsnr=$(echo "$bpsnr" | cut -c -$cha)
				gspsnr=$(echo "$gspsnr" | cut -c -$cha)
				read -r mssim deab mcssim <<< $(echo "$out" | gawk '
				match($0, /MSSIM = (.+)$/, m) {m1=m[1]}
				match($0, /dE\*ab = (.+)$/, m) {m2=m[1]}
				match($0, /MCSSIM = (.+)$/, m) {m3=m[1]}
				END {print m1, m2, m3;}')
				mssim=$(echo "$mssim" | cut -c -$cha)
				deab=$(echo "$deab" | cut -c -$cha)
				mcssim=$(echo "$mcssim" | cut -c -$cha)
				echo "MOS="
				read mos
				echo ""
				eval "echo \"$i & $j & $k & Essai $n & $eqm & $reqm & $geqm & $beqm & $gseqm & $snr & $rsnr & $gsnr & $bsnr & $gssnr & $psnr & $rpsnr & $gpsnr & $bpsnr & $gspsnr & $mssim & $deab & $mcssim & $mos \\\\\" >> tableau.txt"
				n=$(($n + 1))
				k=$(($k - 1))
			done
			j=$(($j - 1))
			k=$6
		done
		i=$(($i - 1))
		j=$5
	done
fi